﻿namespace Synergy.AWSTest
{
    class Config
    {
        public string RegionName { get; set;}
        public string QueueName { get; set; }
        public string TopicName { get; set; }
        public string BucketName { get; set; }
        public string ObjectName { get; set; }
    }
}