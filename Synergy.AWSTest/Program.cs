﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using MessageAttributeValue = Amazon.SimpleNotificationService.Model.MessageAttributeValue;

namespace Synergy.AWSTest
{
    class Test
    {
        private static IConfiguration _configuration;

        private static ILogger _logger;

        private static Config _applicationConfig;

        public TimeReport TimeReport = new TimeReport();

        static async Task Main(string[] args)
        {
            Console.WriteLine("Application started.");

            Console.WriteLine("Creating logger.");

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables()
                .Build();

            var serviceCollection = new ServiceCollection();
            serviceCollection.AddLogging(x =>
            {
                x.AddConfiguration(_configuration.GetSection("Logging"));
                x.AddConsole();
            });

            var serviceProvider = serviceCollection.BuildServiceProvider();

            using (var scope = serviceProvider.CreateScope())
            {
                _logger = scope.ServiceProvider.GetRequiredService<ILogger<Test>>();

                _logger.LogInformation("Logger created.");

                _applicationConfig = _configuration.Get<Config>();

                _logger.LogInformation($"Configuration loaded. {JsonConvert.SerializeObject(_applicationConfig)}");

                var r = new List<TimeReport>();

                var testCount = 100;
                for (var i = 0; i < testCount; i++)
                {
                    _logger.LogInformation($"Starting test {i + 1} of {testCount}.");
                    var test = new Test();
                    await test.RunAsync();
                    r.Add(test.TimeReport);
                }

                var maxTimings = new TimeReport()
                {
                    CreateQueue = r.Max(x => x.CreateQueue),
                    CreateTopicTime = r.Max(x => x.CreateTopicTime),
                    GetObjectTime = r.Max(x => x.GetObjectTime),
                    ReceiveMessageTime = r.Max(x => x.ReceiveMessageTime),
                    SendMessageTime = r.Max(x => x.SendMessageTime)
                };

                var minTimings = new TimeReport()
                {
                    CreateQueue = r.Min(x => x.CreateQueue),
                    CreateTopicTime = r.Min(x => x.CreateTopicTime),
                    GetObjectTime = r.Min(x => x.GetObjectTime),
                    ReceiveMessageTime = r.Min(x => x.ReceiveMessageTime),
                    SendMessageTime = r.Min(x => x.SendMessageTime)
                };

                var avgTimings = new TimeReport()
                {
                    CreateQueue = TimeSpan.FromMilliseconds(r.Average(x => x.CreateQueue.TotalMilliseconds)),
                    CreateTopicTime = TimeSpan.FromMilliseconds(r.Average(x => x.CreateTopicTime.TotalMilliseconds)),
                    GetObjectTime = TimeSpan.FromMilliseconds(r.Average(x => x.GetObjectTime.TotalMilliseconds)),
                    ReceiveMessageTime = TimeSpan.FromMilliseconds(r.Average(x => x.ReceiveMessageTime.TotalMilliseconds)),
                    SendMessageTime = TimeSpan.FromMilliseconds(r.Average(x => x.SendMessageTime.TotalMilliseconds)),
                };

                _logger.LogInformation("=================== All tests execution finished =========================");

                _logger.LogInformation($"Results(Max): {JsonConvert.SerializeObject(maxTimings)}");
                _logger.LogInformation($"Results(Min): {JsonConvert.SerializeObject(minTimings)}");
                _logger.LogInformation($"Results(Avg): {JsonConvert.SerializeObject(avgTimings)}");

                
                

            }

            Console.WriteLine("Exiting");

            Console.Out.Flush();
        }

        async Task RunAsync()
        {
            
            var region = RegionEndpoint.GetBySystemName(_applicationConfig.RegionName);
            
            var sw = new Stopwatch();

            try
            {
                sw.Start();
                var (client, topicArn) = await CreateSNSTopicAsync(region, _applicationConfig.TopicName);
                this.TimeReport.CreateTopicTime = sw.Elapsed;
                _logger.LogDebug("SNS topic created.");
                sw.Restart();

                var queueUrl = await CreateSQSQueueAsync(region, _applicationConfig.QueueName, topicArn);
                this.TimeReport.CreateQueue = sw.Elapsed;
                _logger.LogDebug("SQS queue and subscription created.");
                sw.Restart();

                await PublishTestMessageAsync(client, topicArn, new TestMessage
                {
                    Text = "Test message.",
                    PublishDateUtc = DateTime.UtcNow,
                });
                this.TimeReport.SendMessageTime = sw.Elapsed;
                _logger.LogDebug("SNS message published.");
                await Task.Delay(TimeSpan.FromSeconds(2));
                sw.Restart();

                var msg = await ReadSQSMessagesAsync(region, queueUrl);
                this.TimeReport.ReceiveMessageTime = sw.Elapsed;
                _logger.LogDebug($"SQS messages received count {msg.Count()}.");

                var sqs = new AmazonSQSClient(region);
                foreach (var message in msg)
                {
                    _logger.LogDebug($"Message body: {message.Body}");
                    await sqs.DeleteMessageAsync(queueUrl, message.ReceiptHandle);
                }

                sw.Restart();


                await GetAccessAsync(region, _applicationConfig.BucketName, _applicationConfig.ObjectName, default);
                this.TimeReport.GetObjectTime = sw.Elapsed;
                _logger.LogDebug("Object access received.");
                sw.Restart();

                _logger.LogInformation($"Test execution finished. Report: {JsonConvert.SerializeObject(this.TimeReport)}");

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "Application error.");
                throw;
            }
            finally
            {
                Console.Out.Flush();
            }
        }


        static async Task<(AmazonSimpleNotificationServiceClient client, string topicArn)> CreateSNSTopicAsync(
            RegionEndpoint region, string topicName)
        {
            var client = new AmazonSimpleNotificationServiceClient(region);

            var createTopicResponse = await client.CreateTopicAsync(new CreateTopicRequest(topicName), default)
                .ConfigureAwait(false);

            if (createTopicResponse.HttpStatusCode != HttpStatusCode.OK)
                throw new Exception($"Unable to create SNS topic. '{topicName}' Response status {createTopicResponse.HttpStatusCode}");

            return (client, createTopicResponse.TopicArn);
        }

        static async Task PublishTestMessageAsync<T>(AmazonSimpleNotificationServiceClient client, string topicArn, T message)
        {
            var publishResponse = await client.PublishAsync(new PublishRequest
            {
                TopicArn = topicArn,
                Message = JsonConvert.SerializeObject(message),
                MessageAttributes = new Dictionary<string, MessageAttributeValue>
                {
                    {
                        "ClrType", new MessageAttributeValue { DataType = "String", StringValue = typeof(T).FullName }
                    }
                }
            }, default).ConfigureAwait(false);

            if (publishResponse.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Unable send message to SNS. Response status: {publishResponse.HttpStatusCode}" );
            }
        }



        static async Task<string> CreateSQSQueueAsync(RegionEndpoint region, string queueName, string topicArn)
        {
            var client = new AmazonSQSClient(region);

            var createQueueResponse = await client.CreateQueueAsync(
                new CreateQueueRequest(queueName)
                {
                    Attributes = new Dictionary<string, string>
                    {
                        {
                            "VisibilityTimeout", (30).ToString(CultureInfo.InvariantCulture)
                        }
                    }
                }, default).ConfigureAwait(false);


            if (createQueueResponse.HttpStatusCode != HttpStatusCode.OK)
                throw new Exception($"Unable get or create SQS url. Response code: {createQueueResponse.HttpStatusCode}");

            var snsClient = new AmazonSimpleNotificationServiceClient(region);
            var subscriptionArn = await snsClient.SubscribeQueueAsync(topicArn, client, createQueueResponse.QueueUrl).ConfigureAwait(false);

            var filter = JsonConvert.SerializeObject(new
            {
                ClrType = new[] { typeof(TestMessage).FullName }
            });

            await snsClient.SetSubscriptionAttributesAsync(subscriptionArn, "FilterPolicy", filter, default).ConfigureAwait(false);

            return createQueueResponse.QueueUrl;
        }

        static async Task<IEnumerable<Message>> ReadSQSMessagesAsync(RegionEndpoint region, string queueUrl)
        {
            var client = new AmazonSQSClient(region);

            var receiveResponse = await client.ReceiveMessageAsync(new ReceiveMessageRequest
            {
                MaxNumberOfMessages = 10,
                QueueUrl = queueUrl,
                WaitTimeSeconds = 20,
                AttributeNames = new List<string> {"All"}

            }, default).ConfigureAwait(false);

            if (receiveResponse.HttpStatusCode != HttpStatusCode.OK)
                throw new Exception($"Unable to read SQS message. Response code: {receiveResponse.HttpStatusCode}");

            return receiveResponse.Messages;
        }

        public static async Task<IEnumerable<(string path, string url)>> GetAccessAsync(RegionEndpoint region, string bucketName, string path, CancellationToken cancellationToken = default(CancellationToken))
        {
            var amazonS3 = new AmazonS3Client(region);

            var keysRequest = new ListObjectsRequest
            {
                BucketName = bucketName,
                Prefix = path,
                MaxKeys = 5
            };

            var list = new List<(string path, string url)>();

            var response = await amazonS3.ListObjectsAsync(keysRequest, cancellationToken).ConfigureAwait(false);
            foreach (var obj in response.S3Objects.Where(x => x.Size > 0))
            {
                var url = amazonS3.GetPreSignedURL(new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = obj.Key,
                    Verb = HttpVerb.GET,
                    Expires = DateTime.Now.AddSeconds(30),
                    Protocol = Protocol.HTTPS
                });

            
                var info = await amazonS3.GetObjectMetadataAsync(new GetObjectMetadataRequest { BucketName = bucketName, Key = obj.Key }, cancellationToken).ConfigureAwait(false);

                list.Add((path: obj.Key, url: url));
            }

            
            return list;
        }
    }
}
