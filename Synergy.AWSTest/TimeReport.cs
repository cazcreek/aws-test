﻿using System;

namespace Synergy.AWSTest
{
    class TimeReport
    {
        public TimeSpan CreateTopicTime { get; set; }
        public TimeSpan GetObjectTime { get; set; }
        public TimeSpan ReceiveMessageTime { get; set; }
        public TimeSpan CreateQueue { get; set; }
        public TimeSpan SendMessageTime { get; set; }
    }
}