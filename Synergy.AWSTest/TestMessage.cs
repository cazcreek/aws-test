﻿using System;

namespace Synergy.AWSTest
{
    class TestMessage
    {
        public string Text { get; set; }
        public DateTime PublishDateUtc { get; set; }
    }
}