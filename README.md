## Purpose:

Test some AWS services performance.

## Items to be tested:

* SNS topic (create/check existance)
* SQS queue (create/check existance) and subscription creation
* Send message to SNS topic
* Read message from SQS
* Read file metadata and generate presigned URL for existing S3 object

## Build and run

* Clone repository
* Set Synergy.AWSTest as current directory
* Build docker image
* Start in ECS (or another way) with environment variables:
	* ``RegionName`` _(default: us-east-1)_ 
  * ``QueueName`` _(default: test-aws-queue-app)_ **will be created automatically**
  * ``TopicName`` _(default: test-aws-topic-app)_ **will be created automatically**
  * ``BucketName`` _(default: test-attachments)_ **existing bucket**
  * ``ObjectPrefix`` _(default: none)_ **object prefix to search in S3 bucket. Up to 5 objects will be selected on each test iteration. Leave empty te get any 5 objects from bucket**
* **(Optional)** AWS credentials can be provided as environment variables: ``AWS_ACCESS_KEY_ID``, ``AWS_SECRET_ACCESS_KEY`` 

## Execution

During execution application will write logs to the container ``STDOUT`` so they should be stored somewhere e.g. in CloudWatch stream. 
Example:
```
      Starting test 98 of 100.
info: Synergy.AWSTest.Test[0]
      SNS topic created.
info: Synergy.AWSTest.Test[0]
      SQS queue and subscription created.
info: Synergy.AWSTest.Test[0]
      SNS message published.
info: Synergy.AWSTest.Test[0]
      SQS messages received count 1.
dbug: Synergy.AWSTest.Test[0]
      Message body: {
        "Type" : "Notification",
        "MessageId" : "6be6c221-9397-50c9-802c-9772072bb47a",
        "TopicArn" : "arn:aws:sns:us-east-1:146647109252:test-aws-topic-app",
        "Message" : "{\"Text\":\"Test message.\",\"PublishDateUtc\":\"2019-05-13T15:51:24.4016824Z\"}",
        "Timestamp" : "2019-05-13T15:51:24.490Z",
        "SignatureVersion" : "1",
        "MessageAttributes" : {
          "ClrType" : {"Type":"String","Value":"Synergy.AWSTest.TestMessage"}
        }
      }
info: Synergy.AWSTest.Test[0]
      Object access received.
info: Synergy.AWSTest.Test[0]
      Test execution finished. Report: {"CreateTopicTime":"00:00:00.1408849","GetObjectTime":"00:00:00.2715840","ReceiveMessageTime":"00:00:00.1666021","ReceiveMessagesCount":1,"CreateQueue":"00:00:00.7379938","SendMessageTime":"00:00:00.1564920"}
```
**After 100 test iterations application will exit and log overall test results (min,max, avg) times of each tested operation in ``STDOUT`` like this:**

```
=================== All tests execution finished =========================
info: Synergy.AWSTest.Test[0]
      Results(Max): {"CreateTopicTime":"00:00:01.1289460","GetObjectTime":"00:00:01.2696603","ReceiveMessageTime":"00:00:00.3529873","ReceiveMessagesCount":1,"CreateQueue":"00:00:02.2941276","SendMessageTime":"00:00:00.3446328"}
info: Synergy.AWSTest.Test[0]
      Results(Min): {"CreateTopicTime":"00:00:00.1266185","GetObjectTime":"00:00:00.2616608","ReceiveMessageTime":"00:00:00.1497531","ReceiveMessagesCount":1,"CreateQueue":"00:00:00.6963435","SendMessageTime":"00:00:00.1387352"}
info: Synergy.AWSTest.Test[0]
      Results(Avg): {"CreateTopicTime":"00:00:00.1510000","GetObjectTime":"00:00:00.2990000","ReceiveMessageTime":"00:00:00.1830000","ReceiveMessagesCount":1,"CreateQueue":"00:00:00.8190000","SendMessageTime":"00:00:00.1580000"}
Exiting

```


